﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using InspectionApp.DBHelpers;
using InspectionApp.Droid.Helpers;
using System.IO;

[assembly: Xamarin.Forms.Dependency(typeof(DatabasePath))]
namespace InspectionApp.Droid.Helpers
{
    public class DatabasePath : IDBPath
    {

        public DatabasePath()
        {
        }

        public string GetDbPath()
        {
            return Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "MB_BASSnet.db");
        }
    }
}