//using CommonModel;

using SQLitePCL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using InspectionApp.Models;
using SQLite;


namespace InspectionApp.DataHelper
{
    public class DataBase
    {
        #region Constant Declaration

        public static string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        public static string dbName = @"MB_BASSnet.db";

        decimal REC_ACTIVE = 0;
        const string USER_ID = "USER_ID";

        #endregion

        #region Attribute Declaration

        //private MaterialRepository materialRepo;
       // private List<Material> materials;
        //private BASS_MTRLSTOCK mtrlStocks;
        #endregion

        #region Public Method
        public void InitDatabase()
        {
        }
        public BASS_VESSEL GetVesselId(string VESSEL_CODE)
        {
            try
            {
                
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbName)))
                {
                    var columnInfo = connection.GetTableInfo("BASS_VESSEL");
                    return connection.Table<BASS_VESSEL>().Where(t => t.DB_ID == VESSEL_CODE).FirstOrDefault();

                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }


        public bool updateUser(BASS_SEC_USER entUser)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbName)))
                {
                    int success = connection.Update(entUser);
                    if (success == 0)
                        return false;

                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public bool insertUser(BASS_SEC_USER entUser)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbName)))
                {
                    int success = connection.Insert(entUser);
                    if (success == 0)
                        return false;

                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public bool IsUserEmpty()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbName)))
                {
                    var bASS_SEC_USER = connection.Table<BASS_SEC_USER>();
                    if (bASS_SEC_USER == null)
                        return true;
                    if (bASS_SEC_USER.Count() == 0)
                        return true;

                    return false;
                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return true;
            }
        }

        public BASS_SEC_USER GetUser(string USER_ID)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbName)))
                {                     
                   // return null;
                    var bASS_SEC_USER = connection.Table<BASS_SEC_USER>().Where(t => t.USERID == USER_ID && t.REC_DELETED == REC_ACTIVE).FirstOrDefault();
                   return bASS_SEC_USER;
                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public bool Authenticate(string userId, string password)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbName)))
                {
                    //var bASS_SEC_VESSEL = GetVesselId("BASS");
                    var bASS_SEC_USER = GetUser(userId);

                    if (bASS_SEC_USER != null)
                    {
                        if (!VerifyHash(password, bASS_SEC_USER.PASSWORD))
                        {
                            return false;
                        }
                        else
                        {
                            //bASS_SEC_USER.LOGINTIMESTAMP = DateTime.UtcNow;
                            connection.Commit();

                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        //public BASS_DBS_DATABASEINFO GetBassDbsDatabaseInfo(string id)
        //{
        //    try
        //    {
        //        using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbName)))
        //        {
        //            return connection.Table<BASS_DBS_DATABASEINFO>().Where(t => t.DEVICE_ID == id).FirstOrDefault();
        //        }
        //    }
        //    catch (SQLiteException ex)
        //    {
        //        Log.Info("SQLiteEx", ex.Message);
        //        return null;
        //    }
        //}

        //public bool UpdateBassDbsDatabaseInfo(BASS_DBS_DATABASEINFO bASS_DBS_DATABASEINFO)
        //{
        //    try
        //    {
        //        using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbName)))
        //        {
        //            connection.Query<BASS_DBS_DATABASEINFO>("UPDATE BASS_DBS_DATABASEINFO SET IP_ADDRESS =?, DB_SYNCSEQ =?,AUTO_SYNC_DATA =?, " +
        //                                "VESSELTRX_ID =?, VESSEL_ID =?, USERID =?, DB_SYNCDATE =?, REC_REVISOR =?, REC_REVDATE =?" +
        //                                "WHERE DEVICE_ID=?", bASS_DBS_DATABASEINFO.IP_ADDRESS, bASS_DBS_DATABASEINFO.DB_SYNCSEQ, bASS_DBS_DATABASEINFO.AUTO_SYNC_DATA,
        //                                bASS_DBS_DATABASEINFO.VESSELTRX_ID, bASS_DBS_DATABASEINFO.VESSEL_ID, bASS_DBS_DATABASEINFO.USERID, bASS_DBS_DATABASEINFO.DB_SYNCDATE,
        //                                bASS_DBS_DATABASEINFO.REC_REVISOR, DateTime.Now, Android.OS.Build.Id);
        //            return true;
        //        }
        //    }
        //    catch (SQLiteException ex)
        //    {
        //        Log.Info("SQLiteEx", ex.Message);
        //        return false;
        //    }
        //}

        //public bool InsertBassDbsDatabaseInfo(BASS_DBS_DATABASEINFO bASS_DBS_DATABASEINFO)
        //{
        //    try
        //    {
        //        using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, dbName)))
        //        {
        //            var bassDbsDatabaseInfo = connection.Table<BASS_DBS_DATABASEINFO>().Where(t => t.DEVICE_ID == Android.OS.Build.Id).FirstOrDefault();

        //            if (bassDbsDatabaseInfo == null)
        //            {
        //                bASS_DBS_DATABASEINFO.DEVICE_ID = Android.OS.Build.Id;
        //                bASS_DBS_DATABASEINFO.REC_CREDATE = DateTime.Now;
        //                connection.Insert(bASS_DBS_DATABASEINFO);
        //            }
        //            return true;
        //        }
        //    }
        //    catch (SQLiteException ex)
        //    {
        //        Log.Info("SQLiteEx", ex.Message);
        //        return false;
        //    }
        //}

        public void DeleteDatabase()
        {
            try
            {
                File.Delete(Path.Combine(folder, dbName));
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Method
        private static bool VerifyHash(string password, string hashPassword)
        {
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            using (SHA256 mSHA256 = new SHA256Managed())
            {
                byte[] mBytes = Encoding.ASCII.GetBytes(password);
                string base64Password = Convert.ToBase64String(mSHA256.ComputeHash(mBytes));

                return (comparer.Compare(base64Password, hashPassword) == 0);

            }
        }
        #endregion

    }

}