﻿
using System;
using System.Collections.Generic;
using System.Text;
using SQLite;


namespace InspectionApp.Persistence
{
    public interface ISQLiteDb
    {
        SQLiteAsyncConnection GetConnection();
    }
}
