﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;
using InspectionApp.DBHelpers;
using InspectionApp.Models;


namespace InspectionApp.DataAccess
{
    public class DatabaseContext : DbContext
    {      
        public DbSet<BASS_REG_FINDING> bASS_REG_FINDINGs { get; set; }
       

        public DatabaseContext()
        {
            this.Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            var dbPath = DependencyService.Get<IDBPath>().GetDbPath();
            optionsBuilder.UseSqlite($"Filename={dbPath}");
        }
    }
}
