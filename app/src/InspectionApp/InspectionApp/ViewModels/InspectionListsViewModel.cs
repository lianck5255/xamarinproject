﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;

using InspectionApp.DataHelper;
using SQLite;
using InspectionApp.Models;
using InspectionApp.Views;


namespace InspectionApp.ViewModels
{
    public class InspectionListsViewModel : INotifyPropertyChanged
    {
        decimal REC_ACTIVE = 0;
        private INavigation navigation;
        //  public event PropertyChangedEventHandler PropertyChanged = delegate { };
        //  public ObservableCollection<BASS_REG_InspectionLists> InspectionListsmodel { get; set; }

        public ObservableCollection<BASS_RPT_MAIN> inspectionlistsmodel { get; set; }
        

        public INavigation Navigation
        {
            get { return navigation; }
            set { navigation = value; }
        }

        public InspectionListsViewModel(INavigation _navigation)
        {
            navigation = _navigation;
            inspectionlistsmodel = GetInspetionLists();
        }

        public ObservableCollection<BASS_RPT_MAIN> GetInspetionLists()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(DataBase.folder, DataBase.dbName)))
                {
                    var bASS_INSPECTION_LISTS = connection.Table<BASS_RPT_MAIN>().Where(t => t.REC_DELETED == REC_ACTIVE).ToList();
                    return new ObservableCollection<BASS_RPT_MAIN>(bASS_INSPECTION_LISTS);
                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
