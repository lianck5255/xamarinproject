﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using InspectionApp.Models;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using InspectionApp.Views;

namespace InspectionApp.ViewModels
{

    public class ModulesViewModel : INotifyPropertyChanged
    {
        #region Fields
        private Command<object> tapCommand;
        private INavigation navigation;
        #endregion

        public ObservableCollection<MODULE_MODEL> modulemodel { get; set; }       

        public Command<object> TapCommand
        {
            get { return tapCommand; }
            set { tapCommand = value; }
        }
        public INavigation Navigation
        {
            get { return navigation; }
            set { navigation = value; }
        }


        #region Constructor

        public ModulesViewModel(INavigation _navigation)
        {
            navigation = _navigation;
            //modulemodel = new ObservableCollection<MODULE_MODEL>();
            Random r = new Random();
            tapCommand = new Command<object>(OnTapped);

            modulemodel = new ObservableCollection<MODULE_MODEL>() {
                 new MODULE_MODEL()
                    {
                        MODULE_ID = "1",
                        MODULE_IMAGE = "Finding24.png",
                        MODULE_NAME = "Findings"
                    },

                    new MODULE_MODEL()
                    {
                        MODULE_ID = "2",
                        MODULE_IMAGE = "Inspection24.png",
                        MODULE_NAME = "Inspections"
                    },

                    new MODULE_MODEL()
                    {
                        MODULE_ID = "3",
                        MODULE_IMAGE = "Audit24.png",
                        MODULE_NAME = "Audits"
                    },

                    new MODULE_MODEL()
                    {
                        MODULE_ID = "4",
                        MODULE_IMAGE = "Vetting24.png",
                        MODULE_NAME = "Vetting"
                    },

                    new MODULE_MODEL()
                    {
                        MODULE_ID = "5",
                        MODULE_IMAGE = "Setting24.png",
                        MODULE_NAME = "Setting"
                    }
            };
          
        }

        private void OnTapped(object obj)
        {


            if (obj.ToString() == "Findings")
            {
                var findPage = new FindingPage();
                Navigation.PushAsync(findPage);
            }
            else if (obj.ToString() == "Inspections")
            {
                var findingPage2 = new FindingPage2();
                Navigation.PushAsync(findingPage2);
            }
            else if (obj.ToString() == "Audit")
            {
                var auditPage = new ImageEditorPage("pic7.png");
                Navigation.PushAsync(auditPage);
            }
            else if (obj.ToString() == "Settings")
            {
                var cameraPage = new CameraPage();
                Navigation.PushAsync(cameraPage); 
            }
        }

        #endregion

        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
      

    }
}
