﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;

using InspectionApp.DataHelper;
using SQLite;
using InspectionApp.Models;
using InspectionApp.Views;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using InspectionApp.ViewModels;

namespace InspectionApp.ViewModels
{
    public class VesselOwnerViewModel : INotifyPropertyChanged
    {
        decimal REC_ACTIVE = 0;
        private INavigation navigation;
        int vesselSelectedIndex = 0;

        public ICommand GoToAddItemCommand { get; }
        public ICommand GoToEditItemCommand { get; }
        public ICommand GoToDeleteItemCommand { get; }
        //  public Command GoToEditItemCommand { get; }
        //public Command DeleteItemsCommand { get; }

        public ObservableCollection<BASS_VESSEL_OWNER> vesselownermodel { get; set; }
        public ObservableCollection<string> vesselListviewModel { get; }
        private ObservableCollection<object> selectedItems;
        string stmtfindinglist = "SELECT RF.FINDING_ID, RF.DESCRIPTION, RF.REC_REVDBID, RF.REC_REPLDATE,RF.REC_DELETED, RF.REC_CREATOR, RF.REC_CREDATE, RF.REC_REVISOR, RF.REC_REVDATE,VO.VESSEL_CODE, VO.VESSEL_NAME, VO.VESSEL_ID FROM BASS_REG_FINDING RF LEFT JOIN BASS_VESSEL_OWNER VO ON(RF.REC_REVDBID = VO.VESSEL_ID)WHERE VO.REC_DELETED = 0 AND RF.REC_DELETED = 0 AND VO.VESSEL_NAME = ?";

        public ObservableCollection<BASS_REG_FINDING> findingmodel2
        {
            get => findingmodel21;
            set
            {
                findingmodel21 = value;
                OnPropertyChanged(nameof(findingmodel2));
            }
        }


        string _selectedItem;
        private ObservableCollection<BASS_REG_FINDING> findingmodel21;

        private object selectedItem;
        public object SelectedItem
        {
            get { return this.selectedItem; }
            set
            {
                this.selectedItem = value;
                this.OnPropertyChanged("SelectedItem");
            }
        }

        //public string SelectedItem
        //{
        //    get => _selectedItem;
        //    set
        //    {
        //        _selectedItem = value;
        //        OnPropertyChanged(nameof(SelectedItem));
        //        findingmodel2 = GetFindings(SelectedItem);
        //    }
        //}

      //  public string SelectedItemsValue => string.IsNullOrEmpty(SelectedItem) ? "" : "Selected Item: " + SelectedItem;

        public INavigation Navigation
        {
            get { return navigation; }
            set { navigation = value; }
        }

        public int VesselSelectedIndexChanged
        {
            get
            {
                return vesselSelectedIndex;
            }
            set
            {
                if (vesselSelectedIndex != value)
                {
                    vesselSelectedIndex = value;

                    // trigger some action to take such as updating other labels or fields
                    OnPropertyChanged(nameof(VesselSelectedIndexChanged));

                    //findingmodel2 = GetFindings("ACSU");// ctedIndex];
                }
            }
        }

        public ObservableCollection<BASS_REG_FINDING> GetFindings(string dbID)
        {
            try
            {

                using (var connection = new SQLiteConnection(System.IO.Path.Combine(DataBase.folder, DataBase.dbName)))
                {
                    var bASS_REG_FINDING = connection.Query<BASS_REG_FINDING>(stmtfindinglist, dbID);
                    //var bASS_REG_FINDING = connection.Table<BASS_REG_FINDING>().Where(t => t.REC_DELETED == REC_ACTIVE && t.VESSEL_NAME == dbID).ToList();
                    return new ObservableCollection<BASS_REG_FINDING>(bASS_REG_FINDING);
                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public VesselOwnerViewModel(INavigation _navigation)
        {
            navigation = _navigation;

            vesselListviewModel = new ObservableCollection<string>
            {
                "MT Asian Feeder",
                "Achilles Sun",
                "Apollo Century I",
                "BASS Alpha",
                "BASS Pioneer",
                "Galaxy of the Sea"
            };


            findingmodel2 = GetFindings("MT Asian Feeder");

            this.GoToAddItemCommand = new Command(this.GoToAddItem);
            this.GoToEditItemCommand = new Command(this.GoToEditItem);
            this.GoToDeleteItemCommand = new Command(this.GoToDeleteItem);
            // this.GoToEditItemCommand = new Command(this.GoToEditItem, this.CanExecuteGoToEditItemCommand);
            //this.DeleteItemsCommand = new Command(this.DeleteItems, this.CanExecuteDeleteItemsCommand);
        }

        public ObservableCollection<object> SelectedItems
        {
            get
            {
                return this.selectedItems;
            }
            set
            {
                if (this.selectedItems != value)
                {
                    ObservableCollection<object> oldValue = this.selectedItems;
                    this.selectedItems = value;
                    this.OnSelectedItems(oldValue);
                    this.OnPropertyChanged(("SelectedItems"));
                }
            }
        }

        private void GoToAddItem()
        {

            var addnewfindingPage = new AddFindingPage();
            Navigation.PushAsync(addnewfindingPage);

            

            //var findingDetailsPage = new FindingDetailsPage();
            //Navigation.PushAsync(findingDetailsPage);
            //AddItemViewModel addItemViewModel = new AddItemViewModel(this.OrderDetails);
            //this.NavigationService.NavigateToConfigurationAsync(addItemViewModel);
        }

        private void GoToDeleteItem()
        {
        }


        private void GoToEditItem()
        {
            var findingDetailsPage = new FindingDetailsPage();
            Navigation.PushAsync(findingDetailsPage);
            //EditItemViewModel editItemViewModel = new EditItemViewModel((Order)this.selectedItems[0]);
            //this.NavigationService.NavigateToConfigurationAsync(editItemViewModel);
        }

        //private void GoToEditItem(object arg)
        //{
        //    //EditItemViewModel editItemViewModel = new EditItemViewModel((Order)this.selectedItems[0]);
        //    //this.NavigationService.NavigateToConfigurationAsync(editItemViewModel);
        //}

        private bool CanExecuteGoToEditItemCommand(object arg)
        {
            bool canExecute = this.selectedItems != null && this.selectedItems.Count == 1;
            return canExecute;
        }

        private void SelectedItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.UpdateCanExecuteGoToEditItemCommand();
            this.UpdateCanExecuteDeleteItemsCommand();
        }

        private void OnSelectedItems(ObservableCollection<object> oldValue)
        {
            if (oldValue != null)
            {
                oldValue.CollectionChanged -= this.SelectedItems_CollectionChanged;
            }

            if (this.selectedItems != null)
            {
                this.selectedItems.CollectionChanged += this.SelectedItems_CollectionChanged;
            }

            this.UpdateCanExecuteGoToEditItemCommand();
            this.UpdateCanExecuteDeleteItemsCommand();
        }

        private void DeleteItems(object arg)
        {
            List<object> selectedItemsCopy = new List<object>(this.selectedItems);
            //foreach (Order item in selectedItemsCopy)
            //{
            //    this.OrderDetails.Remove(item);
            //}
        }

        private void UpdateCanExecuteGoToEditItemCommand()
        {
            //this.GoToEditItemCommand.ChangeCanExecute();
        }

        private void UpdateCanExecuteDeleteItemsCommand()
        {
           // this.DeleteItemsCommand.ChangeCanExecute();
        }

        private bool CanExecuteDeleteItemsCommand(object arg)
        {
            bool canExecute = this.selectedItems != null && this.selectedItems.Count > 0;
            return canExecute;
        }
        //async Task ExecuteLoadItemsCommand()
        //{
        //    if (IsBusy)
        //        return;

        //    IsBusy = true;

        //    try
        //    {
        //        var items = await DataStore.GetItemsAsync(true);
        //        AllItems.ReplaceRange(items);
        //        FilterItems();
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex);
        //    }
        //    finally
        //    {
        //        IsBusy = false;
        //    }
        //}



        //void FilterItems()
        //{
        //    Items.ReplaceRange(AllItems.Where(a => a.Role == SelectedFilter || SelectedFilter == "All"));
        //}

        public ObservableCollection<BASS_VESSEL_OWNER> GetVesselOwner()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(DataBase.folder, DataBase.dbName)))
                {
                    var bASS_VESSEL_OWNER = connection.Table<BASS_VESSEL_OWNER>().Where(t => t.REC_DELETED == REC_ACTIVE).ToList();
                    return new ObservableCollection<BASS_VESSEL_OWNER>(bASS_VESSEL_OWNER);
                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }



        //#region INotifyPropertyChanged     
        //public event PropertyChangedEventHandler PropertyChanged;
        //protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}
        //#endregion

        #region Interface Member
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
