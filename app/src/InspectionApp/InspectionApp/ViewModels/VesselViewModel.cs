﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;

using InspectionApp.DataHelper;
using SQLite;
using InspectionApp.Models;
using InspectionApp.Views;


namespace InspectionApp.ViewModels
{
    public class VesselViewModel : INotifyPropertyChanged
    {
        decimal REC_ACTIVE = 0;
        private INavigation navigation;      
        
        public ObservableCollection<BASS_VESSEL> findingmodel { get; set; }


        public INavigation Navigation
        {
            get { return navigation; }
            set { navigation = value; }
        }

        public VesselViewModel(INavigation _navigation)
        {
            navigation = _navigation;
            findingmodel =  GetVessels();
        }

        public ObservableCollection<BASS_VESSEL> GetVessels()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(DataBase.folder, DataBase.dbName)))
                {
                    var bASS_VESSEL = connection.Table<BASS_VESSEL>().Where(t => t.REC_DELETED == REC_ACTIVE).ToList();
                    return new ObservableCollection<BASS_VESSEL>(bASS_VESSEL);
                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
