﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using InspectionApp.DataHelper;
using SQLite;
using InspectionApp.Models;
using InspectionApp.Views;


namespace InspectionApp.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private INavigation navigation;
        public Action DisplayInvalidLoginPrompt;
        public Action DisplayInvalidPasswordPrompt;
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private string userid;
        decimal REC_ACTIVE = 0;

        public INavigation Navigation
        {
            get { return navigation; }
            set { navigation = value; }
        }

        public string UserId
        {
            get { return userid; }
            set
            {
                userid = value;
                PropertyChanged(this, new PropertyChangedEventArgs("UserId"));
            }
        }
        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Password"));
            }
        }
        public ICommand SubmitCommand { protected set; get; }
        public LoginViewModel(INavigation _navigation)
        {
            navigation = _navigation;
            SubmitCommand = new Command(OnSubmit);
        }


        public BASS_SEC_USER GetUser(string USER_ID)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(DataBase.folder, DataBase.dbName)))
                {
                    // return null;
                    var bASS_SEC_USER = connection.Table<BASS_SEC_USER>().Where(t => t.USERID == USER_ID && t.REC_DELETED == REC_ACTIVE).FirstOrDefault();
                    return bASS_SEC_USER;
                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public void OnSubmit()
        {

            try
            {

                //temporary skip password checking
                //var MmoduleCollectionPage = new ModuleCollectionPage();
                //Navigation.PushAsync(MmoduleCollectionPage);
                
                var modulesPage = new ModulesPage();
                Navigation.PushAsync(modulesPage);
                //var bASS_SEC_USER = GetUser(userid);
                //if (bASS_SEC_USER != null)
                //{
                //    if (!VerifyHash(password, bASS_SEC_USER.PASSWORD))
                //    {
                //        DisplayInvalidPasswordPrompt();
                //    }
                //    else
                //    {                      

                //        var modulesPage = new ModulesPage();
                //        Navigation.PushAsync(modulesPage);

                //    }
                //}
                //else
                //{
                //    DisplayInvalidLoginPrompt();
                //}

            }
            catch
            {
                throw;
            }
        }

        private static bool VerifyHash(string password, string hashPassword)
        {
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            using (SHA256 mSHA256 = new SHA256Managed())
            {
                byte[] mBytes = Encoding.ASCII.GetBytes(password);
                string base64Password = Convert.ToBase64String(mSHA256.ComputeHash(mBytes));

                return (comparer.Compare(base64Password, hashPassword) == 0);

            }
        }
    }
}
