﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

using Xamarin.Forms;
using InspectionApp.DataHelper;
using SQLite;
using InspectionApp.Models;
using InspectionApp.Views;
using InspectionApp.ViewModels;
using System.Runtime.CompilerServices;
using System.Windows.Input;


namespace InspectionApp.ViewModels
{
    public class FindingDetailsViewModel : INotifyPropertyChanged
    {
        
        private INavigation navigation;
        private Command<object> tapCommand;
        private Command<object> tapImageCommand;
        private Command<object> tapActionCommand;
        private Command tapAddCommand;
        private Command tapCancelCommand;
        private Command tapAddNewFindingCommand;

        public ObservableCollection<BASS_REG_FINDING> findingmodel { get; set; }

        public ObservableCollection<FindingActions> findingActionviewModel { get; set; }
        public ObservableCollection<BASS_REG_FINDING_IMAGES> imagemodel { get; set; }

        public INavigation Navigation
        {
            get { return navigation; }
            set { navigation = value; }
        }
              

        public Command<object> TapActionCommand
        {
            get { return tapActionCommand; }
            set { tapActionCommand = value; }
        }

        public Command TapAddCommand
        {
            get { return tapAddCommand; }
            set { tapAddCommand = value; }
        }

        public Command TapCancelCommand
        {
            get { return tapCancelCommand; }
            set { tapCancelCommand = value; }
        }

        public Command<object> TapCommand
        {
            get { return tapCommand; }
            set { tapCommand = value; }
        }

        public Command<object> TapImageCommand
        {
            get { return tapImageCommand; }
            set { tapImageCommand = value; }
        }

        public Command TapAddNewFindingCommand
        {
            get { return tapAddNewFindingCommand; }
            set { tapAddNewFindingCommand = value; }
        }


        public FindingDetailsViewModel(INavigation _navigation)
        {
            navigation = _navigation;
            Random r = new Random();

            tapAddNewFindingCommand = new Command(OnTapAddFindingRec);

            tapCommand = new Command<object>(OnTapped);
            tapImageCommand = new Command<object>(onTapImageCommand);
            tapActionCommand = new Command<object>(OnActionTapped);

            findingActionviewModel = new ObservableCollection<FindingActions>
            {
                 new FindingActions()
                    {
                        ActionId = "1",
                        ActionTitle = "Camera",
                    },

                 new FindingActions()
                    {
                        ActionId = "2",
                        ActionTitle = "Video Recoding",
                    },

                 new FindingActions()
                    {
                        ActionId = "3",
                        ActionTitle = "Voice Recording",
                    },
            };
            

            imagemodel = new ObservableCollection<BASS_REG_FINDING_IMAGES>() {
                 new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "101",
                        IMAGE_NAME = "",
                        IMAGE_TYPE = "Photo",
                        DESCRIPTION = "Date of the last Port State Inspection is not updated in the system",
                        FINDING_CONTENTS = "+ Add"
                    },

                 new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "1",
                        IMAGE_NAME = "pic2.png",
                        IMAGE_TYPE = "Photo",
                        DESCRIPTION = "Last 10 Port of Call List not updated for ISPS records",
                        FINDING_CONTENTS = ""
                    },

                   new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "02",
                        IMAGE_NAME = "BASSnet.mp4",
                        IMAGE_TYPE = "Photo",
                        DESCRIPTION = "Class survey reports not adequately filled",
                        FINDING_CONTENTS = "Video"
                    },

                    new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "2",
                        IMAGE_NAME = "pic3.png",
                        IMAGE_TYPE = "Photo",
                        DESCRIPTION = "Drug & Alcohol Test for the current crew onboard not completed",                        
                        FINDING_CONTENTS = ""
                    },

                    new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "3",
                        IMAGE_NAME = "pic4.png",
                        IMAGE_TYPE = "Photo",
                        DESCRIPTION = "GPS not adjusted to the current Datum",
                        FINDING_CONTENTS = ""
                    },

                     new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "103",
                        IMAGE_NAME = "",
                        IMAGE_TYPE = "Photo",
                        DESCRIPTION = "Slandering order or night order not issued regularly by the master",
                        FINDING_CONTENTS = "Attachment"
                    },

                    new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "4",
                        IMAGE_NAME = "pic5.png",
                        IMAGE_TYPE = "Photo",
                        DESCRIPTION = "Slandering order or night order not issued regularly by the master",
                        FINDING_CONTENTS = ""
                    },

                    new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "5",
                        IMAGE_NAME = "pic6.png",
                        DESCRIPTION = "Maritime safety information from NAVTEX or EGC not checked and recorded regularly",
                        IMAGE_TYPE = "Photo",
                        FINDING_CONTENTS = ""
                    },

                    new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "6",
                        IMAGE_NAME = "pic7.png",
                        IMAGE_TYPE = "Photo",
                        DESCRIPTION = "Maritime safety information from NAVTEX or EGC not checked and recorded regularly",
                        FINDING_CONTENTS = ""
                    },

                    new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "7",
                        IMAGE_NAME = "pic8.png",
                        IMAGE_TYPE = "Setting",
                        DESCRIPTION = "Operating instruction of steering changeover not clearly posted in the Steering Gear Room",                        
                        FINDING_CONTENTS = ""
                    },

                    new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "ISM01",
                        IMAGE_NAME = "ISM01.jpg",
                        IMAGE_TYPE = "Setting",
                        DESCRIPTION = "Updated watch scheduled for watch-keepers not posted on the Bridge",
                        FINDING_CONTENTS = ""
                    },

                    new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "ISM02",
                        IMAGE_NAME = "ISM02.jpg",
                        IMAGE_TYPE = "Setting",
                        DESCRIPTION = "Emergency evacuation route found secured and not accessible",
                        FINDING_CONTENTS = ""
                    },

                    new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "ISM03",
                        IMAGE_NAME = "ISM03.jpg",
                        IMAGE_TYPE = "Setting",
                        DESCRIPTION = "MLC complaint record not properly maintained – closed outs missing",
                        FINDING_CONTENTS = ""
                    },

                    new BASS_REG_FINDING_IMAGES()
                    {
                        IMAGE_ID = "ISM04",
                        IMAGE_NAME = "ISM04.jpg",
                        IMAGE_TYPE = "Setting",
                        DESCRIPTION = "6 monthly frequency testing for SSAS is overdue by 1 week",
                        FINDING_CONTENTS = ""
                    }
            };

            
        }

        private void OnTapAddFindingRec()
        {
            var newdata = new BASS_REG_FINDING_IMAGES()
            {
                IMAGE_ID = "New001",
                IMAGE_NAME = "MainPageBackGroundPort.png",
                IMAGE_TYPE = "Setting",
                DESCRIPTION = "New record",
                FINDING_CONTENTS = ""
            };

            imagemodel.Add(newdata);
           
        }

        //private void AddCommand()
        //{
        //}

        private void CancelCommand()
        {
        }

        private void OnTapped(object obj)
        {
            if (obj.ToString() == "BASSnet.mp4")
            {
                //var addfindingPage = new AddFindingPage();
                //Navigation.PushAsync(addfindingPage); 
            }
            else if (obj.ToString() == "")
            {
                var addfindingPage = new AddFindingPage();
                Navigation.PushAsync(addfindingPage);
            }
            else
            {
                var imageEditorPage = new ImageSFEditorPage(obj.ToString());
                Navigation.PushAsync(imageEditorPage);
            }        
         
        }

        private void onTapImageCommand(object obj)
        {
           
        }

        private void OnActionTapped(object obj)
        {

            if (obj.ToString() == "takecamera")
            {
            }
            else if (obj.ToString() == "voicerecording")
            {
            }
            else if (obj.ToString() == "recording")
            {
            }           
        }


        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
