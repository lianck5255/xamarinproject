﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;

using InspectionApp.DataHelper;
using SQLite;
using InspectionApp.Models;
using InspectionApp.Views;


namespace InspectionApp.ViewModels
{
    public class FindingViewModel : INotifyPropertyChanged
    {
        decimal REC_ACTIVE = 0;
        private INavigation navigation;
        //  public event PropertyChangedEventHandler PropertyChanged = delegate { };
        //  public ObservableCollection<BASS_REG_FINDING> findingmodel { get; set; }        
        public ObservableCollection<BASS_REG_FINDING> findingmodel 
        { get; set; }

        public INavigation Navigation
        {
            get { return navigation; }
            set { navigation = value; }
        }

        public FindingViewModel()
        {
            findingmodel = GetFindings();
        }

        public FindingViewModel(INavigation _navigation)
        {
            navigation = _navigation;
            findingmodel =  GetFindings();
        }

        public ObservableCollection<BASS_REG_FINDING> GetFindings()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(DataBase.folder, DataBase.dbName)))
                {                    
                    var bASS_REG_FINDING = connection.Table<BASS_REG_FINDING>().Where(t => t.REC_DELETED == REC_ACTIVE).ToList();
                    return new ObservableCollection<BASS_REG_FINDING>(bASS_REG_FINDING);
                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
