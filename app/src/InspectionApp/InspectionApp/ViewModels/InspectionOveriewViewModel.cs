﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

using Xamarin.Forms;
using InspectionApp.DataHelper;
using SQLite;
using InspectionApp.Models;
using InspectionApp.Views;
using InspectionApp.ViewModels;
using System.Runtime.CompilerServices;
using System.Windows.Input;


namespace InspectionApp.ViewModels
{
    public class InspectionOverviewViewModel : INotifyPropertyChanged
    {
        
        private INavigation navigation;
        private Command<object> tapCommand;     
      

        public ObservableCollection<InspectionOverview> inspectionoverviewmodel { get; set; }
               
      

        public Command<object> TapCommand
        {
            get { return tapCommand; }
            set { tapCommand = value; }
        }

        public INavigation Navigation
        {
            get { return navigation; }
            set { navigation = value; }
        }

        public InspectionOverviewViewModel(INavigation _navigation)
        {
            navigation = _navigation;
            Random r = new Random();
            tapCommand = new Command<object>(OnTapped);
           

            inspectionoverviewmodel = new ObservableCollection<InspectionOverview>
            {
                new InspectionOverview("Compliance", 40),
                new InspectionOverview("Non-Conformities", 4),
                new InspectionOverview("Deficiencies", 4),
                new InspectionOverview("Observations", 32),
                new InspectionOverview("Not Applicable", 1)
            };        
            
        }

        private void OnTapped(object obj)
        {            
            var imageEditorPage = new ImageSFEditorPage(obj.ToString());
            Navigation.PushAsync(imageEditorPage);           
         
        }       


        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
