﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

using Xamarin.Forms;
using InspectionApp.DataHelper;
using SQLite;
using InspectionApp.Models;
using InspectionApp.Views;
using InspectionApp.ViewModels;
using System.Runtime.CompilerServices;
using System.Windows.Input;


namespace InspectionApp.ViewModels
{
    public class CheckListsItemsViewModel : INotifyPropertyChanged
    {

        public ObservableCollection<BASS_CHECKLIST_ITEMS> checklistitemsviewmodel { get; set; }
                                   

        public CheckListsItemsViewModel()
        {
          
            checklistitemsviewmodel = new ObservableCollection<BASS_CHECKLIST_ITEMS>
            {
                new BASS_CHECKLIST_ITEMS()
                {
                    CheckListSeqNo = 1,
                    CheckListDesc = "Do the operators procedures manuals comply with ISM Code Requirements?",
                    CheckListComment = "",
                    IsSelectedCheck1 = false,
                    IsSelectedCheck2 = false,
                    IsSelectedCheck3 = false,
                    IsSelectedCheck4 = false,
                    IsSelectedCheck5 = false
                },
                new BASS_CHECKLIST_ITEMS()
                {
                    CheckListSeqNo = 2,
                    CheckListDesc = "Does the operators representative visit the vessel at least bi-annually?",
                    CheckListComment = "",
                    IsSelectedCheck1 = false,
                    IsSelectedCheck2 = false,
                    IsSelectedCheck3 = false,
                    IsSelectedCheck4 = false,
                    IsSelectedCheck5 = false
                },
                new BASS_CHECKLIST_ITEMS()
                {
                    CheckListSeqNo = 3,
                    CheckListDesc = "Is a recent operators audit report available and is a close-out system in place for dealing with non-conformities?",
                    CheckListComment = "",
                    IsSelectedCheck1 = false,
                    IsSelectedCheck2 = false,
                    IsSelectedCheck3 = false,
                    IsSelectedCheck4 = false,
                    IsSelectedCheck5 = false
                }
            };
        }

        

        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
