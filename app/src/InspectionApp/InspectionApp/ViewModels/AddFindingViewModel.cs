﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using InspectionApp.DBHelpers;
using InspectionApp.Models;
using InspectionApp.DataAccess;
using SQLite;
using InspectionApp.DataHelper;
using InspectionApp.Views;


namespace InspectionApp.ViewModels
{
    public class AddFindingViewModel : BaseViewModel
    {
        public ObservableCollection<BASS_REG_FINDING> findingheadermodel
        { get; set; }

        private string finding_id;

        public string FINDING_ID
        {
            get { return finding_id; }
            set
            {
                finding_id = value;
                OnPropertyChanged();
            }
        }

        private string description;
        public string DESCRIPTION
        {
            get { return description; }
            set
            {
                description = value;
                OnPropertyChanged();
            }
        }

        public ICommand SaveFindingCommand { get; private set; }

        //private string _findingId;
        private DatabaseContext _context;

        public AddFindingViewModel()
        {
            //_context = new DatabaseContext();
            //SaveFindingCommand = new Command(SaveFinding);
        }

        void SaveFinding()
        {

            var _bass_reg_finding = new BASS_REG_FINDING 
            { 
                FINDING_ID = "9999",
                DESCRIPTION = DESCRIPTION
                
            };

            _context.bASS_REG_FINDINGs.Add(_bass_reg_finding);
            _context.SaveChanges();

            Application.Current.MainPage.Navigation.PopAsync();
        }

        public BASS_REG_FINDING SaveFindingHeader(BASS_REG_FINDING2 bASS_REG_FINDING)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(DataBase.folder, DataBase.dbName)))
                {
                    // var bassRegFindingInfo = connection.Table<BASS_REG_FINDING>().Where(t => t.FINDING_ID == "999d").FirstOrDefault();
                    //BASS_REG_FINDING2 _bass_reg_finding2 = new BASS_REG_FINDING2
                    //{
                    //    FINDING_ID = Guid.NewGuid().ToString(),
                    //    DESCRIPTION = "Finding002",
                    //    REC_CREATOR = "BASSADM",
                    //    REC_REVISOR = "BASSADM",
                    //    REC_DELETED = 0,
                    //    REC_REPLDATE = DateTime.Now,
                    //    REC_REVDATE = DateTime.Now,
                    //    REC_CREDATE = DateTime.Now,
                    //};
                    //FindingActions _bassAction = new FindingActions
                    //{
                    //    ActionId = "Act001",
                    //    ActionTitle = "Click",
                    //};
                    connection.Insert(bASS_REG_FINDING);
                  
           
                    // string querystring = "INSERT INTO BSF.BASS_REG_FINDING_IMAGES (IMAGE_ID, IMAGE_NAME,FINDING_ITEM_ID,IMAGE_TYPE, FINDING_IMAGE, REC_DELETED, REC_REVDBID, REC_REVISOR, REC_CREATOR, REC_CREDATE, REC_REVDATE, REC_REPLDATE) " +
                    //              "VALUES (" + @IMAGE_ID + ", @IMAGE_NAME,@FINDING_ITEM_ID,@IMAGE_TYPE, @FINDING_IMAGE, @REC_DELETED, @REC_REVDBID, @REC_REVISOR, @REC_CREATOR, @REC_CREDATE, @REC_REVDATE, @REC_REPLDATE)";

                    //connection.Query<BASS_REG_FINDING>(querystring);

                    //connection.Insert(bASS_REG_FINDING);
                    //connection.Query<BASS_REG_FINDING>("INSERT BASS_REG_FINDING SET FINDING_ID  = @FINDING_ID , DESCRIPTION =?," +
                    //                    "REC_DELETED =?, REC_REVDBID =?, REC_REVISOR =?, REC_CREATOR =?, " +
                    //                    "REC_CREDATE =?, REC_REVDATE =?, REC_REPLDATE =?  " +
                    //                    bASS_REG_FINDING.FINDING_ID, bASS_REG_FINDING.DESCRIPTION
                    //                    , 0, bASS_REG_FINDING.REC_REVDBID, bASS_REG_FINDING.REC_REVISOR, bASS_REG_FINDING.REC_CREATOR,
                    //                    DateTime.Now, DateTime.Now, DateTime.Now);

                    return null;// bASS_REG_FINDING_IMAGES;
                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return null;
            }

        }
            public BASS_REG_FINDING_IMAGES SaveFindingRecords(BASS_REG_FINDING_IMAGES bASS_REG_FINDING_IMAGES)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(DataBase.folder, DataBase.dbName)))
                {
                    connection.Query<BASS_REG_FINDING_IMAGES>("INSERT BASS_REG_FINDING_IMAGES SET IMAGE_ID =?, IMAGE_NAME =?,FINDING_ITEM_ID =?, " +
                                        "IMAGE_TYPE =?, FINDING_IMAGE =?, REC_DELETED =?, REC_REVDBID =?, REC_REVISOR =?, REC_CREATOR =?, " +
                                        "REC_CREDATE =?, REC_REVDATE =?, REC_REPLDATE =?  " +
                                        bASS_REG_FINDING_IMAGES.IMAGE_ID, bASS_REG_FINDING_IMAGES.IMAGE_NAME, bASS_REG_FINDING_IMAGES.FINDING_ITEM_ID,
                                        bASS_REG_FINDING_IMAGES.IMAGE_TYPE, bASS_REG_FINDING_IMAGES.FINDING_IMAGE, 0, bASS_REG_FINDING_IMAGES.REC_REVDBID, bASS_REG_FINDING_IMAGES.REC_REVISOR, bASS_REG_FINDING_IMAGES.REC_CREATOR, 
                                        DateTime.Now, DateTime.Now, DateTime.Now);

                    return null;// bASS_REG_FINDING_IMAGES;
                }
            }
            catch (SQLiteException ex)
            {
                //Log.Info("SQLiteEx", ex.Message);
                return null;
            }

        
        //            connection.Query<BASS_DBS_DATABASEINFO>("UPDATE BASS_DBS_DATABASEINFO SET IP_ADDRESS =?, DB_SYNCSEQ =?,AUTO_SYNC_DATA =?, " +
        //                                "VESSELTRX_ID =?, VESSEL_ID =?, USERID =?, DB_SYNCDATE =?, REC_REVISOR =?, REC_REVDATE =?" +
        //                                "WHERE DEVICE_ID=?", bASS_DBS_DATABASEINFO.IP_ADDRESS, bASS_DBS_DATABASEINFO.DB_SYNCSEQ, bASS_DBS_DATABASEINFO.AUTO_SYNC_DATA,
        //                                bASS_DBS_DATABASEINFO.VESSELTRX_ID, bASS_DBS_DATABASEINFO.VESSEL_ID, bASS_DBS_DATABASEINFO.USERID, bASS_DBS_DATABASEINFO.DB_SYNCDATE,
        //                                bASS_DBS_DATABASEINFO.REC_REVISOR, DateTime.Now, Android.OS.Build.Id);
        //            return true;
        
        }
    }
}
