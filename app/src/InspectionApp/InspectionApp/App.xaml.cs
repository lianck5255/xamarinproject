﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InspectionApp
{
	public partial class App : Application
	{
		public App()
		{
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MTI5OTU2QDMxMzcyZTMyMmUzMGJKOEFicWxURmVtY1RQeXIvaWl2K1pjbDgyUDMxVGdDNlZsUG1SUTBnSU09");
            InitializeComponent();

#if DEBUG
            HotReloader.Current.Run(this);
#endif
           
            //MainPage = new MainPage();
            MainPage = new NavigationPage(new MainPage());            
        }

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
