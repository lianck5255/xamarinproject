﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;
using Xamarin.Forms;


namespace InspectionApp.CustomControls
{
    [Preserve(AllMembers = true)]
    public class CustomEntry : Entry
    {
        public Syncfusion.SfPdfViewer.XForms.SfPdfViewer PdfViewer
        {
            get;
            set;
        }

        public bool IsPageNumberEntry
        {
            get; set;
        }

        public CustomEntry()
        {
        }
    }
}