﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SQLite;

namespace InspectionApp.Models
{
    /// <summary>
    /// A simple business object
    /// </summary>
    public class BASS_REG_FINDING : ObservableCollection<BASS_REG_FINDING>
    {
        //[SQLite.PrimaryKey]
        [PrimaryKey, AutoIncrement]
        public string FINDING_ID { get; set; }

        public string DESCRIPTION { get; set; }
        //public string CONTENTS { get; set; }
        public string REC_CREATOR { get; set; }
        public DateTime REC_CREDATE { get; set; }
        public DateTime REC_REVDATE { get; set; }
        public string REC_REVISOR { get; set; }        
        public decimal REC_DELETED { get; set; }
        public DateTime REC_REPLDATE { get; set; }
        public string REC_REVDBID { get; set; }
        //public string VESSEL_CODE { get; set; }
        //public string VESSEL_NAME { get; set; }
        //public string VESSEL_ID { get; set; }
        //public string IMAGE { get; set; }
    }
}
