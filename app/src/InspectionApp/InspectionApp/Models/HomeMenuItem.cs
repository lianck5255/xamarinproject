﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InspectionApp.Models
{
    public enum MenuItemType
    {
        //Browse,
        //About
        Finding,
        Inspection,
        Audit,
        Vetting,
        Setting,
        About
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
