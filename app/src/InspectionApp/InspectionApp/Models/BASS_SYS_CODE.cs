﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspectionApp.Models
{
    public class BASS_SYS_CODE : ObservableCollection<BASS_SYS_CODE>
    {
        public string SYSCODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_FULL { get; set; }
        public string REC_ID { get; set; }
        public string SET_DEFAULT { get; set; }
        public int TREE_LEVEL { get; set; }
        public int SEQ_NO { get; set; }
        public string PARENT_REC { get; set; }
        public string DEFAULT_STRUCTURE { get; set; }
        public string SYSCODE_TYPE_INEXTERNAL { get; set; }
        public string ADDRESS_ID { get; set; }
        public int INCL_NC { get; set; }
        public int INCL_OBS { get; set; }
        public int INCL_DEF { get; set; }
        public string AUINSVET_CHECKLISTTMPL_RECID { get; set; }
        public int AUINSVET_IS_LOCKED { get; set; }
        public string SELF_CHECKLISTTMPL_RECID { get; set; }
        public int SELF_IS_LOCKED { get; set; }
    }
}
