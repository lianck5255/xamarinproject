﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspectionApp.Models
{
    public class BASS_REG_CHECKLIST_TMPL : ObservableCollection<BASS_REG_CHECKLIST_TMPL>
    {
        public string CHECKLISTTMPL_RECID { get; set; }
        public string DESCRIPTION { get; set; }
        public int INACTIVE { get; set; }
    }
}
