﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspectionApp.Models
{
    public class BASS_REG_CHECKLIST_TMPL_ITEM : ObservableCollection<BASS_REG_CHECKLIST_TMPL_ITEM>
    {
        public string CHECKLISTTMPL_ITEM_RECID { get; set; }
        public string CHECKLISTTMPL_SECT_RECID { get; set; }
        public string CHECKLISTITEM_RECID { get; set; }
        public string CHECKLISTITEM_CODE { get; set; }
        public string CHECKLISTITEM_RECID_DESC { get; set; }
        public int SEQ_NUMBER { get; set; }
        public string COMMENT { get; set; }
        public int COMPLIANCE { get; set; }
    }
}
