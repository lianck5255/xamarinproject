using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspectionApp.Models
{
    public class BASS_VESSEL_OWNER : ObservableCollection<BASS_VESSEL_OWNER>
    {
        [SQLite.PrimaryKey]
        public string VESSEL_ID { get; set; }
        public string VESSEL_CODE { get; set; }
        public string VESSELTRX_ID { get; set; }
        public string VESSEL_NAME { get; set; }
        public string VESSEL_NAME_ABBR { get; set; }
        public int REC_DELETED { get; set; }
        public string REC_REVDBID { get; set; }
        public string REC_CREATOR { get; set; }
        public DateTime? REC_CREDATE { get; set; }
        public DateTime? REC_REVDATE { get; set; }
        public DateTime? REC_REPLDATE { get; set; }
    }
}
