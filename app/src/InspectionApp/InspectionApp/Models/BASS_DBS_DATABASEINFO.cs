﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using SQLite;

namespace CommonModel
{
    public class BASS_DBS_DATABASEINFO
    {

        public string REC_REVISOR { get; set; }
        public DateTime REC_CREDATE { get; set; }
        public string REC_CREATOR { get; set; }
        public string REC_REVDBID { get; set; }
        public int REC_DELETED { get; set; }
        public string DEVICE_TIMEZONE { get; set; }
        public string VESSELTRX_ID { get; set; }
        public string VESSEL_ID { get; set; }
        public string DBS_WORKSTATION { get; set; }
        public DateTime REC_REVDATE { get; set; }
        public string IP_ADDRESS { get; set; }
        public int DB_SYNCSEQ { get; set; }
        public string DB_PATH { get; set; }
        public string DB_DESCRIPTION { get; set; }
        public string DB_ID { get; set; }
        public int START_SCAN_PAGE { get; set; }
        public int AUTO_SYNC_DATA { get; set; }
        public string USERID { get; set; }
        public string APPPROFILE_ID { get; set; }
        public string DEVICE_ID { get; set; }
        public DateTime DB_SYNCDATE { get; set; }
        public DateTime REC_REPLDATE { get; set; }
    }
}
