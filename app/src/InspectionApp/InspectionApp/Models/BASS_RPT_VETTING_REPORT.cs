﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspectionApp.Models
{
    public class BASS_RPT_VETTING_REPORT : ObservableCollection<BASS_RPT_VETTING_REPORT>
    {
        public string REC_ID { get; set; }
        public string RPT_REC { get; set; }
        public string VETTING_TYPE { get; set; }
        public string VETTING_CODE { get; set; }
        public string INSPECTOR { get; set; }
        public DateTime VETTING_DATE_FROM { get; set; }
        public DateTime VETTING_DATE_TO { get; set; }
    }
}
