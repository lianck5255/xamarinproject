﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspectionApp.Models
{
    public class BASS_REG_INSPECTOR : ObservableCollection<BASS_REG_INSPECTOR>
    {
        public string INSPECTOR_ID { get; set; }
        public string INSPECTOR_NAME { get; set; }
        public int EXTERNAL_INSPECTION { get; set; }
        public int INTERNAL_INSPECTION { get; set; }
    }
}
