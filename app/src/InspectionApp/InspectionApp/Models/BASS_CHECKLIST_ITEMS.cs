﻿using System;

namespace InspectionApp.Models
{

    public class BASS_CHECKLIST_ITEMS
    {
        [SQLite.PrimaryKey]
        public int CheckListSeqNo { get; set; }
        public string CheckListDesc { get; set; }
        public string CheckListComment { get; set; }
        public bool IsSelectedCheck1 { get; set; }
        public bool IsSelectedCheck2 { get; set; }
        public bool IsSelectedCheck3 { get; set; }
        public bool IsSelectedCheck4 { get; set; }
        public bool IsSelectedCheck5 { get; set; }
    }
}