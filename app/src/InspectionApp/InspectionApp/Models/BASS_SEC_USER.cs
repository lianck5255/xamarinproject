﻿using System;
using SQLite;

namespace InspectionApp.Models
{
    public class BASS_SEC_USER
    {
        
        [SQLite.PrimaryKey]
        public string USERID { get; set; }
       // public decimal DEACTIVATED { get; set; }
       // public string RESET_TAG { get; set; }
       // public string RANK_CODE { get; set; }
       // public string FIRSTNAME { get; set; }
       // public string LASTNAME { get; set; }
       // public string TELEPHONE { get; set; }
      //  public string HANDPHONE { get; set; }
       // public string BUSINESS_PHONE { get; set; }
       // public string HOME_PHONE { get; set; }
       // public DateTime LAST_PWD_CHANGED { get; set; }
        public DateTime REC_REVDATE { get; set; }
        public string REC_REVISOR { get; set; }
      //  public string INITIAL { get; set; }
        public DateTime REC_CREDATE { get; set; }
        public decimal REC_DELETED { get; set; }
        public DateTime REC_REPLDATE { get; set; }
        public string REC_REVDBID { get; set; }
        public decimal PROTECTED { get; set; }
        public string SITE { get; set; }
        public string USER_TYPE { get; set; }
        public string EMAIL { get; set; }
      //  public DateTime LOGOUTTIMESTAMP { get; set; }
      //  public DateTime LOGINTIMESTAMP { get; set; }
        public string PASSWORD { get; set; }
        public string NAME { get; set; }
        public string REC_CREATOR { get; set; }
        //public decimal MASKED_FLAG { get; set; }
    }
}
