﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspectionApp.Models
{
    public class BASS_REG_CHECKLIST_TMPL_GROUP : ObservableCollection<BASS_REG_CHECKLIST_TMPL_SECT>
    {
        public string CHECKLISTTMPL_GROUP_RECID { get; set; }
        public string CHECKLISTTMPL_RECID { get; set; }
        public string CHECKLISTGROUP_CODE { get; set; }
        public string CHECKLISTGROUP_DESC { get; set; }
        public int SEQ_NUMBER { get; set; }
    }
}
