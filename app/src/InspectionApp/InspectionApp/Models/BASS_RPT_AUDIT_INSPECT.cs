﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspectionApp.Models
{
    public class BASS_RPT_AUDIT_INSPECT : ObservableCollection<BASS_RPT_AUDIT_INSPECT>
    {
        public string REC_ID { get; set; }
        public string RPT_REC { get; set; }
        public string AUDITINSPECT_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string INSPECTOR { get; set; }
        public DateTime AUDITINSPECT_DATE_FROM { get; set; }
        public DateTime AUDITINSPECT_DATE_TO { get; set; }
        public string AUIS_INSPECTOR { get; set; }
    }
}
