﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspectionApp.Models
{
    public class VettingList
    {
        public string REC_ID { get; set; }
        public string RPT_NO { get; set; }
        public string ITEM_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string STATUS_CODE { get; set; }
        public string VESSEL_ID { get; set; }
        public string VETTING_TYPE { get; set; }
        public string VETTING_TYPE_DESC { get; set; }
        public string VETTING_CODE { get; set; }
        public string VETTING_CODE_DESC { get; set; }
        public DateTime VETTING_DATE_FROM { get; set; }
        public DateTime VETTING_DATE_TO { get; set; }
        public int REC_DELETED { get; set; }
    }
}
