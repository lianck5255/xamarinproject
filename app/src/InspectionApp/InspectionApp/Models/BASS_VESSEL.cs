using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspectionApp.Models
{
    public class BASS_VESSEL : ObservableCollection<BASS_VESSEL>
    {
        [SQLite.PrimaryKey]
        public string VESSEL_ID { get; set; }

        public string VESSELTRX_ID { get; set; }

        public string COMPGRP_ID { get; set; }

        public string DISTGRP_ID { get; set; }

        public string DB_ID { get; set; }

        public string TEMPLATE_ID { get; set; }

        public string VESSEL_IMONO { get; set; }

        public string VESSEL_NAME { get; set; }

        public string VESSEL_TYPE { get; set; }

        public string VESSEL_CLASS { get; set; }

        public string VESSEL_SOCIETYCLASS { get; set; }

        public string VESSEL_FREETEXT { get; set; }

        public int REC_DELETED { get; set; }

        public string REC_REVDBID { get; set; }

        public string REC_CREATOR { get; set; }

        public DateTime? REC_CREDATE { get; set; }

        public DateTime? REC_REVDATE { get; set; }

        public DateTime? REC_REPLDATE { get; set; }

        public string OTHER_VESSEL_NAME { get; set; }

        public decimal? ISVESSEL { get; set; }

        public string REFERENCE { get; set; }
    }
}
