﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InspectionApp.Models
{
    /// <summary>
    /// A simple business object
    /// </summary>
    public class Finding
    {
        public Finding()
        {
        }

        public Finding(string id, string title, string contents, DateTime submissiondate, string image)
        {
            Id = id;
            Title = title;
            Contents = contents;
            SubmissionDate = submissiondate;
            Image = image;
        }

        public string Id { get; set; }
        public string Title { get; set; }
        public string Contents { get; set; }
        public DateTime SubmissionDate { get; set; }
        public string Image { get; set; }
    }

    public class FindingCollection : List<Finding>
    {
    }
}
