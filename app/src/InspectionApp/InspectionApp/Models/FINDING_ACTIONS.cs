﻿using System;

namespace InspectionApp.Models
{
    public class FindingActions
    {
        public string ActionId { get; set; }
        public string ActionTitle { get; set; }
    }
}