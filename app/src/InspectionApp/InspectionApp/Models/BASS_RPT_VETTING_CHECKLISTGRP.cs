﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspectionApp.Models
{
    public class BASS_RPT_VETTING_CHECKLISTGRP : ObservableCollection<BASS_RPT_VETTING_CHECKLISTGRP>
    {
        public string REC_ID { get; set; }
        public string RPT_REC { get; set; }
        public string CHECKLISTTMPL_RECID { get; set; }
        public int SELFASS_CHECKLIST { get; set; }
        public int VETTING_CHECKLIST { get; set; }
    }
}
