﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspectionApp.Models
{
    public class BASS_RPT_MAIN : ObservableCollection<BASS_RPT_MAIN>
    {
        public string REC_ID { get; set; }
        public string RPT_NO { get; set; }
        public string ITEM_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string STATUS_CODE { get; set; }
        public string VESSEL_ID { get; set; }
        public string INVOLVE_INTERNAL { get; set ; }
        public string INVOLVE_EXTERNAL { get; set; }
        public int REC_DELETED { get; set; }
    }
}
