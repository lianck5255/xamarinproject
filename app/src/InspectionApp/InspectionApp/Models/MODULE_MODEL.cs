﻿using System;
using System.Collections.Generic;
using System.Text;


using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace InspectionApp.Models
{
    public class MODULE_MODEL
    {
        public string MODULE_ID { get; set; }
        public string MODULE_NAME { get; set; }
        public string MODULE_IMAGE { get; set; }        
    }
}
