﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SQLite;



namespace InspectionApp.Models
{
    public class BASS_REG_FINDING_IMAGES : ObservableCollection<BASS_REG_FINDING_IMAGES>
    {
        public string IMAGE_ID { get; set; }
        public string IMAGE_NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public string FINDING_ITEM_ID { get; set; }
        public string FINDING_CONTENTS { get; set; }
        public string IMAGE_TYPE { get; set; }
        public string FINDING_IMAGE { get; set; }
        public DateTime REC_REVDATE { get; set; }
        public string REC_REVISOR { get; set; }        
        public DateTime REC_CREDATE { get; set; }
        public decimal REC_DELETED { get; set; }
        public DateTime REC_REPLDATE { get; set; }
        public string REC_REVDBID { get; set; }
        public string REC_CREATOR { get; set; }
    }
}
