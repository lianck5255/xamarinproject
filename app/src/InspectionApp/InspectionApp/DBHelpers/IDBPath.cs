﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InspectionApp.DBHelpers
{
    public interface IDBPath
    {

        string GetDbPath();
    }
}
