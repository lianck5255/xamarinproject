//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: global::Xamarin.Forms.Xaml.XamlResourceIdAttribute("InspectionApp.Views.ImageSFEditorPage.xaml", "Views/ImageSFEditorPage.xaml", typeof(global::InspectionApp.Views.ImageSFEditorPage))]

namespace InspectionApp.Views {
    
    
    [global::Xamarin.Forms.Xaml.XamlFilePathAttribute("Views\\ImageSFEditorPage.xaml")]
    public partial class ImageSFEditorPage : global::Xamarin.Forms.ContentPage {
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::Syncfusion.SfImageEditor.XForms.SfImageEditor SFimageEditor;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private void InitializeComponent() {
            global::Xamarin.Forms.Xaml.Extensions.LoadFromXaml(this, typeof(ImageSFEditorPage));
            SFimageEditor = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::Syncfusion.SfImageEditor.XForms.SfImageEditor>(this, "SFimageEditor");
        }
    }
}
