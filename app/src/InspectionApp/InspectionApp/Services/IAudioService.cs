﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InspectionApp.Services
{
    public interface IAudioService
    {
        void CopyRecordingToExternalStorage(string filePath);
    }
}
