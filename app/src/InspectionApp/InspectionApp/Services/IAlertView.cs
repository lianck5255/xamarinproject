﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InspectionApp.Services
{
    public interface IAlertView
    {
        void Show(string message);
    }
}
