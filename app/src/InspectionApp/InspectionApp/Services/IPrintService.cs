﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace InspectionApp.Services
{
    public interface IPrintService
    {
        void Print(Stream inputStream, string fileName);
    }
}
