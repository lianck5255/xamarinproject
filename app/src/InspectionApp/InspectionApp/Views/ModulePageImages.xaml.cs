﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using InspectionApp.Models;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ModulePageImages : ContentPage
    {
       //private List<Finding> _finding = new List<Finding>();
        public ModulePageImages()
        {
            InitializeComponent();

            //this.BindingContext = new ViewModel();


            //_finding.Add(new Finding("No. 1", "", "Date of the last port state control inspection",
            //                         DateTime.Today.AddDays(-7), ""));
            //_finding.Add(new Finding("No. 2", "", "Port of the last port state control inspection",
            //                          DateTime.Today.AddDays(-5), ""));
            //_finding.Add(new Finding("No. 3", "", "Are Class survey reports adequately field?",
            //                           DateTime.Today.AddDays(-3), ""));
            //_finding.Add(new Finding("No. 4", "", "Is the following documentation available on board?",
            //              DateTime.Today.AddDays(-5), "PS006.png"));
            //_finding.Add(new Finding("No. 5", "", "Has the GP's been adjusted to the correct datum??",
            //                           DateTime.Today.AddDays(-3), ""));


           // colviewModules.ItemsSource = _finding;
            //listView.FilterDescriptors.Add(new Telerik.XamarinForms.DataControls.ListView.DelegateFilterDescriptor { Filter = this.AgeFilter });
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ModulePage());
        }

        private async void TapGestureRecognizer_Tapped_1(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ModulePage());
        }

        private async void TapGestureRecognizer_Tapped_2(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ModulePage());
        }

        private async void TapGestureRecognizer_Tapped_3(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ModulePage());
        }
        private void PeopleListView_ItemSwiping(object sender, Telerik.XamarinForms.DataControls.ListView.ItemSwipingEventArgs e)
        {

        }
        private void PeopleListView_SelectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }

        async void Handle_Tapped(object sender, System.EventArgs e)
        {
            //await ((ViewModel)BindingContext).LoadEmployeeTeamAsync();
        }
    }
}