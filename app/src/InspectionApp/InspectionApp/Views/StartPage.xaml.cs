﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InspectionApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StartPage : ContentPage
	{
		public StartPage()
		{
			InitializeComponent();
		}

		private void OnTapFindingsTapped(object sender, EventArgs e)
		{
			DisplayAlert("Findings", "Hiho", "Ok");
		}

		//private void OnTapInspectionTapped(object sender, EventArgs e)
        private async void OnTapInspectionTapped(object sender, EventArgs e)
        {
            //inspectionApp = new InspectionConPage();
            //var inspectionPage = new InspectionConPage();
            //await this.Navigation.PushAsync(inspectionPage);
            
            //DisplayAlert("Inspection", "Hiho", "Ok");
        }

		private async void OnTapAuditTapped(object sender, EventArgs e)
		{
            //var auditPage = new AuditConPage();
            //await this.Navigation.PushAsync(auditPage);
            //DisplayAlert("Audit", "Hiho", "Ok");
        }

		private void OnTapVettingTapped(object sender, EventArgs e)
		{
			DisplayAlert("Vetting", "Hiho", "Ok");
		}
	}

    public class SourceItem
    {
        public SourceItem(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }
    }

    public class ViewModel
    {
        public ViewModel()
        {
            this.Source = new List<SourceItem> { new SourceItem("Tom"), new SourceItem("Anna"), new SourceItem("Peter"), new SourceItem("Teodor"), new SourceItem("Lorenzo"), new SourceItem("Andrea"), new SourceItem("Martin") };
        }

        public List<SourceItem> Source { get; set; }
    }
}