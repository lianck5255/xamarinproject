﻿using Syncfusion.SfSchedule.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.XamarinForms.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyPlannnerPage : ContentPage
    {

        public MyPlannnerPage()
        {
            InitializeComponent();

            //SfSchedule schedule = new SfSchedule();
            //schedule.ScheduleView = ScheduleView.MonthView;
            //MonthViewSettings monthViewSettings = new MonthViewSettings();
            //monthViewSettings.ShowAgendaView = true;
            //schedule.MonthViewSettings = monthViewSettings;
            //AgendaViewStyle agendaViewStyle = new AgendaViewStyle();
            //// Customize selected Date Text
            //agendaViewStyle.DateFontColor = Color.Purple;
            //agendaViewStyle.HeaderHeight = 40;
            //agendaViewStyle.DateFormat = "dd MMMM, yyyy";
            //agendaViewStyle.DateFontAttributes = FontAttributes.Bold;
            //agendaViewStyle.DateFontSize = 15;
            //agendaViewStyle.DateFontFamily = "Arial";
            //// Customize appointment
            //agendaViewStyle.TimeFontColor = Color.Red;
            //agendaViewStyle.TimeFontSize = 13;
            //agendaViewStyle.TimeFontAttributes = FontAttributes.None;
            //agendaViewStyle.TimeFontFamily = "Arial";
            //agendaViewStyle.TimeFormat = "hh a";
            //agendaViewStyle.SubjectFontColor = Color.Blue;
            //agendaViewStyle.SubjectFontSize = 13;
            //agendaViewStyle.SubjectFontFamily = "Arial";
            //agendaViewStyle.SubjectFontAttributes = FontAttributes.None;
            //agendaViewStyle.BackgroundColor = Color.FromRgb(222, 240, 222);
            //schedule.MonthViewSettings.AgendaViewStyle = agendaViewStyle;


            ScheduleAppointmentCollection appointmentCollection = new ScheduleAppointmentCollection();
            //Creating new event   
            ScheduleAppointment clientMeeting = new ScheduleAppointment();
            DateTime currentDate = DateTime.Now;
            DateTime startTime = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day);
            DateTime endTime = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day + 2);
            clientMeeting.StartTime = startTime;
            clientMeeting.EndTime = endTime;
            clientMeeting.Color = Color.Orange;            
            clientMeeting.Subject = "Inspections\r\nVessel: AFFI";
            clientMeeting.IsAllDay = true;
            clientMeeting.Notes = "AFFI";
            appointmentCollection.Add(clientMeeting);

            ScheduleAppointment clientMeeting2 = new ScheduleAppointment();
            DateTime startTime2 = System.DateTime.Now.AddDays(5);// new DateTime(currentDate.Year, currentDate.Month + 1, currentDate.Day + 5);
            DateTime endTime2 = System.DateTime.Now.AddDays(7); //new DateTime(currentDate.Year, currentDate.Month, currentDate.Day + 7 );
            clientMeeting2.StartTime = startTime2;
            clientMeeting2.EndTime = endTime2;
            clientMeeting2.Color = Color.Red;
            clientMeeting2.IsAllDay = true;
            clientMeeting2.Subject = "Audit\r\nVessel: ACSU";
            clientMeeting2.Notes = "ACSU";
            appointmentCollection.Add(clientMeeting2);

            schedule.ScheduleView = ScheduleView.MonthView;
            MonthViewSettings monthViewSettings = new MonthViewSettings();
            monthViewSettings.ShowAgendaView = true;
            schedule.MonthViewSettings = monthViewSettings;
            
            AgendaViewStyle agendaViewStyle = new AgendaViewStyle();
            // Customize selected Date Text
            agendaViewStyle.DateFontColor = Color.White;
            agendaViewStyle.HeaderHeight = 40;
            agendaViewStyle.DateFormat = "dd MMMM, yyyy";
            agendaViewStyle.DateFontAttributes = FontAttributes.Bold;
            agendaViewStyle.DateFontSize = 15;
            agendaViewStyle.DateFontFamily = "Arial";
            // Customize appointment
            agendaViewStyle.TimeFontColor = Color.White;
            agendaViewStyle.TimeFontSize = 13;
            agendaViewStyle.TimeFontAttributes = FontAttributes.None;
            agendaViewStyle.TimeFontFamily = "Arial";
            agendaViewStyle.TimeFormat = "hh a";
            agendaViewStyle.SubjectFontColor = Color.White;
            agendaViewStyle.SubjectFontSize = 13;
            agendaViewStyle.SubjectFontFamily = "Arial";
            agendaViewStyle.SubjectFontAttributes = FontAttributes.None;
            agendaViewStyle.BackgroundColor = Color.FromHex("#243498");


            schedule.MonthViewSettings.AgendaViewStyle = agendaViewStyle;
            schedule.DataSource = appointmentCollection;
            
            
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            foreach (var tabItem in tabPlanner.Items)
            {
                tabItem.SelectionColor = Color.Red;
                tabItem.TitleFontAttributes = FontAttributes.Bold;
                tabItem.TitleFontSize = Device.Idiom == TargetIdiom.Tablet ? 16 : 12;

            }
        }

       
    }
}