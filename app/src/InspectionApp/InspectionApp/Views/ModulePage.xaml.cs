﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ModulePage : ContentPage
    {
        public ModulePage()
        {
            InitializeComponent();

            var colors = new List<object>();
            for (int i = 0; i < 16; i++)
            {
                var c = 200 - 10 * i;
                colors.Add(new { Color = Color.FromRgb(c, c, c) });
            };

            listViewModule.ItemsSource = colors;
            
        }
    }
}