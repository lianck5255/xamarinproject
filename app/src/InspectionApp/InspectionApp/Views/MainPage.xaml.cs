﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using InspectionApp.DataHelper;
using InspectionApp.Views;

//using InspectionApp.Views;
using InspectionApp.ViewModels;

namespace InspectionApp
{
	// Learn more about making custom code visible in the Xamarin.Forms previewer
	// by visiting https://aka.ms/xamarinforms-previewer
	[DesignTimeVisible(true)]
	public partial class MainPage : ContentPage
	{
        private DataBase database = new DataBase();
        const string USER_ID = "USER_ID";
        const string BASSADM = "BASSADM";

        public MainPage()
		{
			InitializeComponent();

            var vm = new LoginViewModel(this.Navigation);
            this.BindingContext = vm;
            vm.DisplayInvalidLoginPrompt += () => DisplayAlert("Error", "Invalid User ID, try again", "OK");
            vm.DisplayInvalidPasswordPrompt += () => DisplayAlert("Error", "Invalid Password, try again", "OK");


            txtUser.Completed += (object sender, EventArgs e) =>
            {
                txtUser.Focus();
            };

            txtPassword.Completed += (object sender, EventArgs e) =>
            {
                vm.SubmitCommand.Execute(null);
            };
        }


		//public static readonly BindableProperty HorizontalTextAlignmentProperty = BindableProperty.Create(nameof(ITextAlignmentElement.HorizontalTextAlignment), typeof(TextAlignment), typeof(Button), TextAlignment.Center, propertyChanged: OnHorizontalTextAlignmentPropertyChanged);

		private async void CmdLogin_Clicked(object sender, EventArgs e)
		{
            // TODO: Validate user login
            try
            {
                #region Login via Database
               
                if (database.Authenticate(txtUser.Text, txtPassword.Text))
                {      
                //var modulePage = new ModulePage();
                //await this.Navigation.PushAsync(modulePage);

                    var moduleCollectPage = new ModuleCollectionPage();
                    await this.Navigation.PushAsync(moduleCollectPage);

                //var appModulePageImages = new AppModulesPage();
                //await this.Navigation.PushAsync(appModulePageImages);


                //var modulePageImages = new ModulePageImages();
                //await this.Navigation.PushAsync(modulePageImages);

                //var startPage = new StartPage();
                //    await this.Navigation.PushAsync(startPage);                    
                }
                else
                {
                    throw new Exception("Invalid user id or password");
                }
                #endregion
            }
            catch (Exception ex)
            {
                //Log.Info("Login failed", ex.Message);
                //Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
            }
            //var startPage = new StartPage();
            //await this.Navigation.PushAsync(startPage);

        }
	}
}
