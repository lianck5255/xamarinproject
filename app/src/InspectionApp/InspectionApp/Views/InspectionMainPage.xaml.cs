﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using InspectionApp.Models;
using InspectionApp.ViewModels;
using InspectionApp.Views;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InspectionMainPage : ContentPage
    {
        public InspectionMainPage()
        {
            InitializeComponent();
            //BindingContext = new InspectionOverviewViewModel(this.Navigation);
            Chart.BindingContext = new InspectionOverviewViewModel(this.Navigation);
            CheckListListView.BindingContext = new CheckListsItemsViewModel();

        }

        async void ExportButtonClicked(object sender, EventArgs e)
        {
            string action = await DisplayActionSheet("Export to?", "Cancel", null, ".DOC", ".PDF");
        }

        private void Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            var container = BindingContext as CheckListsItemsViewModel;

            CheckListListView.BeginRefresh();

            if (string.IsNullOrWhiteSpace(e.NewTextValue))
            {
                CheckListListView.ItemsSource = container.checklistitemsviewmodel;
            }
            else
            {
                CheckListListView.ItemsSource = container.checklistitemsviewmodel.Where(i => i.CheckListDesc.ToLower().Contains(e.NewTextValue.ToLower()));
            }

            CheckListListView.EndRefresh();
        }
    }
}