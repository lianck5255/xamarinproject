﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.DataControls.ListView;
using Telerik.XamarinForms.Primitives;
using Plugin.Media;
using Plugin.Media.Abstractions;

namespace HSEQ_First_Mock
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public IList<Models.SourceItem> Items { get; private set; }
        public MainPage()
        {
            InitializeComponent();

            Items = new List<Models.SourceItem>();

            Items.Add(new Models.SourceItem
            {
                Name = "1.Do the operator procedures manuals comply with ISM Code Requirements?"
            });

            Items.Add(new Models.SourceItem
            {
                Name = "2.Does the operators representative visit the vessel at least bi-annually?"
            });

            Items.Add(new Models.SourceItem
            {
                Name = "3.Is a recent operators audit report available and is a close-out system in the place for dealing with non-conformities?"
            });

            BindingContext = this;
        }
        /*
        private async void TakePhotoButton_Clicked (object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("No Camera", "No Camera Available", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = true,
                Name = "Test.jpg"
            });

            if (file == null)
            {
                return;
            }

            Image1.Source = ImageSource.FromStream(() => file.GetStream());
            */
        
    }
}
