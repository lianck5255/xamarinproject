﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSEQ_First_Mock.Models
{
    public class SourceItem
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
