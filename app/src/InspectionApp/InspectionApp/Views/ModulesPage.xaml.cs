﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using InspectionApp.ViewModels;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class ModulesPage : ContentPage
    {

        private double width;
        private double height;

        public ModulesPage()
        {
            InitializeComponent();

            //lblcurrentdatetime.Text = DateTime.Now.ToString();
            //NavigationPage.SetHasBackButton(this, true);
            
            ((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.White;
            ((NavigationPage)Application.Current.MainPage).BarTextColor = Color.Black;

            BindingContext = new ModulesViewModel(this.Navigation);
        }


        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (width != this.width || height != this.height)
            {
                this.width = width;
                this.height = height;
                if (width > height)
                {
                    if (Device.RuntimePlatform == Device.Android)
                        this.BackgroundImageSource = ImageSource.FromFile("drawable/MainPageBackGround.png");
                    else
                        this.BackgroundImageSource = ImageSource.FromFile("Resources/MainPageBackGround.png");
                    // this.BackgroundImageSource = ImageSource.FromFile("drawable/MainPageBackGround.png");

                }
                else
                {
                    if (Device.RuntimePlatform == Device.Android)
                        this.BackgroundImageSource = ImageSource.FromFile("drawable/MainPageBackGroundPort.png");
                    else
                        this.BackgroundImageSource = ImageSource.FromFile("Resources/MainPageBackGroundPort.png");
                    // this.BackgroundImageSource = ImageSource.FromFile("drawable/MainPageBackGroundPort.png");

                }
            }


        }

        private void BtnFinding_Clicked(object sender, EventArgs e)
        {

            //Navigation.PushAsync(new FindingPage2());
            activity.IsEnabled = true;
            activity.IsRunning = true;
            activity.IsVisible = true;
            Navigation.PushAsync(new FindingPage2());
            //Device.BeginInvokeOnMainThread(async () =>
            //{
            //    await Navigation.PushAsync(new FindingPage2());
            //});
            //activity.IsEnabled = false;
            activity.IsRunning = false;
            activity.IsVisible = false;
        }

        private void BtnPlanner_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MyPlannnerPage());
        }

        //private void BtnDashboard_Clicked(object sender, EventArgs e)
        //{
        //    Navigation.PushAsync(new InspectionMainPage());
        //}

        private void BtnInspection_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new InspectionMainPage());
        }

        //private void BtnReport_Clicked(object sender, EventArgs e)
        //{
        //    Navigation.PushAsync(new ReportPage());
        //}

        private void BtnOverview_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new FindingCardViewPage());
        }

        private void BtnAudit_Clicked(object sender, EventArgs e)
        {

        }

        private void BtnVetting_Clicked(object sender, EventArgs e)
        {

        }

        private void BtnSetting_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SettingPage());
        }
    }
}