﻿using Syncfusion.SfImageEditor.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImageSFEditorPage : ContentPage
    {
        public ImageSFEditorPage(string imagesource)
        {
            InitializeComponent();
            SFimageEditor.Source = imagesource;
        }
    }
}


