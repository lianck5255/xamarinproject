﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using InspectionApp.ViewModels;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FindingCardViewPage : ContentPage
    {
        public FindingCardViewPage()
        {
            BindingContext = new FindingDetailsViewModel(this.Navigation);
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // BindingContext = new AddFindingViewModel();
        }

        private void ImageValidation(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {

        }
    }
}