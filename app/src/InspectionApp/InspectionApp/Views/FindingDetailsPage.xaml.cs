﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using InspectionApp.ViewModels;
using Plugin.Media;
using Plugin.Media.Abstractions;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FindingDetailsPage : ContentPage
    {
        public FindingDetailsPage()
        {
            InitializeComponent();


            //For edit
            BindingContext = new FindingDetailsViewModel(this.Navigation);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

           // BindingContext = new AddFindingViewModel();
        }

        private async void Btnrecord_Clicked(object sender, EventArgs e)
        {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakeVideoSupported)
            {
                DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakeVideoAsync(new Plugin.Media.Abstractions.StoreVideoOptions
            {
                Name = "video.mp4",
                Directory = "DefaultVideos",
            });

            if (file == null)
                return;

            DisplayAlert("Video Recorded", "Location: " + file.Path, "OK");

            file.Dispose();

        }

        private async void Btncamera_Clicked(object sender, EventArgs e)
        {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                DisplayAlert("No Camera", ":( No camera available.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 75,
                CustomPhotoSize = 50,
                PhotoSize = PhotoSize.MaxWidthHeight,
                MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Front
            });

            if (file == null)
                return;

            DisplayAlert("File Location", file.Path, "OK");

            //image.Source = ImageSource.FromStream(() =>
            //{
            //    var stream = file.GetStream();
            //    file.Dispose();
            //    return stream;
            //});
        }

        private async void Btnvoice_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RecordingPage());
        }
    }
}