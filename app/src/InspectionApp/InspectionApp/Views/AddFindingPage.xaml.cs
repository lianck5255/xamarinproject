﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Media;
using Plugin.Media.Abstractions;
using InspectionApp.ViewModels;
using Syncfusion.SfRotator.XForms;
using System.IO;
using InspectionApp.Models;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddFindingPage : ContentPage
    {
      
        public AddFindingPage()
        {
            InitializeComponent();
            vesselpickerlist.BindingContext = new VesselOwnerViewModel(this.Navigation);

            //var ImageCollection = new List<RotatorModel> {
            //new RotatorModel ("ISM01.jpg"),
            //new RotatorModel ("ISM02.jpg"),
            //new RotatorModel ("ISM03.jpg"),
            //new RotatorModel ("pic3.png"),
            //new RotatorModel ("pic5.jpg"),
            //new RotatorModel ("pic8.png"),
            //new RotatorModel ("pic6.png")
            //};

         ////   var ImageCollection = new FindingDetailsViewModel(this.Navigation).imagemodel;
            //var itemTemplate = new DataTemplate(() =>
            //{
            //    var grid = new Grid();
            //    var nameLabel = new Image();
            //    nameLabel.Aspect = Aspect.AspectFill;
            //    //nameLabel.Opacity = 0.5;
            //    nameLabel.SetBinding(Image.SourceProperty, "IMAGE_NAME");
            //    Label label = new Label();
            //    //label.Text = "Not Available";
            //    label.FontSize = 50;
            //    label.HorizontalOptions = LayoutOptions.EndAndExpand;
            //    label.VerticalOptions = LayoutOptions.EndAndExpand;

            //    nameLabel.HorizontalOptions = LayoutOptions.Center;
            //    nameLabel.VerticalOptions = LayoutOptions.Center;

            //    grid.Children.Add(nameLabel);
            //    grid.Children.Add(label);

            //    return grid;
            //});
            //rotator.ItemTemplate = itemTemplate;
         ////   rotator.NavigationStripMode = NavigationStripMode.Dots;
         ////   rotator.ItemsSource = ImageCollection;
        }

        private async void onCameraButtonClick(object sender, EventArgs e)
        {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                DisplayAlert("No Camera", ":( No camera available.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 75,
                CustomPhotoSize = 50,
                PhotoSize = PhotoSize.MaxWidthHeight,
                MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Front
            });

            if (file == null)
                return;

            //DisplayAlert("File Location", file.Path, "OK");
            
            var stream2 = file.GetStream();
            var ImageCollection = new FindingDetailsViewModel(this.Navigation).imagemodel;
            rotator.NavigationStripMode = NavigationStripMode.Dots;
            rotator.ItemsSource = ImageCollection;

            //byte[] imageAsBytes = null;
            //using (var memoryStream = file.GetStream())
            //{
            //    file.GetStream().CopyTo(memoryStream);
            //    file.Dispose();
            //    imageAsBytes = memoryStream.ToArray();
            //}


            //this.BackgroundImageSource.BindingContext = ImageSource.FromStream(() =>
            //{
            //    var stream = file.GetStream();
            //    file.Dispose();
            //    return stream;
            //});
        }

        private void onVoiceButtonClick(object sender, EventArgs e)
        {
         
        }

        private void onPlayButtonClick(object sender, EventArgs e)
        {
           
        }

        private void onSaveButtonClick(object sender, EventArgs e)
        {
            AddFindingViewModel addFindingVM = new AddFindingViewModel();

            BASS_REG_FINDING2 _bass_reg_finding = new BASS_REG_FINDING2
            {
                FINDING_ID = Guid.NewGuid().ToString(),
                DESCRIPTION = txtFindingDesc.Text.ToString(),
                REC_CREATOR = "BASSADM",
                REC_REVISOR = "BASSADM",
                REC_DELETED = 0,
                REC_REPLDATE = DateTime.Now,
                REC_REVDATE = DateTime.Now,
                REC_CREDATE = DateTime.Now,
            };

            addFindingVM.SaveFindingHeader(_bass_reg_finding);
        }

        private void onCancelButtonClick(object sender, EventArgs e)
        {

        }
    }
}