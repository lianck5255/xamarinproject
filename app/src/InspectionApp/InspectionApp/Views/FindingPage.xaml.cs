﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using InspectionApp.ViewModels;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FindingPage : ContentPage
    {
        //SQLiteAsyncConnection conn = DependencyService.Get<ISqlConnection>().Connection();

        public FindingPage()
        {
            InitializeComponent();
            //var vm = new FindingViewModel(this.Navigation);
            //this.BindingContext = vm;

            BindingContext = new FindingViewModel(this.Navigation);
            //this.BindingContext = this;

            // conn.CreateTableAsync<Contacts>();
            //ReadData();
        }
    }
}