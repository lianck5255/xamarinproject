﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GridListPage : ContentPage
    {
        public List<string> People { get; set; }

        public GridListPage()
        {
            InitializeComponent();
            People = new List<string>
            {
                "Finding",
                "Inspection",
                "Audit",
                "Vetting",
                "Good Received",
                "New Requisition",
                "Reporting Job",                
                "Setting"
            };
            CV.BindingContext = this;
        }
    }
}