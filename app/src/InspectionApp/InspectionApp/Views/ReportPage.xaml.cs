﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Syncfusion.SfPdfViewer.XForms;
using System.Reflection;
using Xamarin.Forms.Internals;
using System.IO;
using InspectionApp.Services;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [Preserve(AllMembers = true)]
    public partial class ReportPage : ContentPage
    {
        private float backUpVerticalOffset = 0;
        private float backUpHorizontalOffset = 0;
        private float backUpZoomFactor = 0;
        //string currentDocument = "FormFillingDocument";
        string currentDocument = "AuditRpt";
        private bool canRestoreBackup = false;
        private bool isPageSwitched = false;

        public ReportPage()
        {
            InitializeComponent();
            //pdfViewerControl.DocumentSaveInitiated += PdfViewerControl_DocumentSaved;
            pdfViewerControl.DocumentLoaded += PdfViewerControl_DocumentLoaded;
           // printButton.Clicked += PrintButton_Clicked;
            //(BindingContext as GettingStartedViewModel).DocumentName = currentDocument;
            isPageSwitched = true;
        }

        private void PrintButton_Clicked(object sender, EventArgs e)
        {
            
            Stream printStream = pdfViewerControl.SaveDocument();
            DependencyService.Get<IPrintService>().Print(printStream, "BNAudit.pdf");
            //DependencyService.Get<IPrintService>().Print(printStream, "GIS Succinctly.pdf");
        }

        private void PdfViewerControl_DocumentLoaded(object sender, EventArgs args)
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                if (canRestoreBackup)
                {
                    pdfViewerControl.ZoomPercentage = backUpZoomFactor;
                    pdfViewerControl.VerticalOffset = backUpVerticalOffset;
                    pdfViewerControl.HorizontalOffset = backUpHorizontalOffset;
                    canRestoreBackup = false;
                }
            }
        }
        protected override void OnAppearing()
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                string filePath = string.Empty;
#if COMMONSB
            filePath = "SampleBrowser.Samples.PdfViewer.Samples.";

#else
                //filePath = "SampleBrowser.SfPdfViewer.";
                filePath = "InspectionApp.";
                Stream fileStream;
#endif
                canRestoreBackup = !isPageSwitched;

                fileStream = typeof(App).GetTypeInfo().Assembly.GetManifestResourceStream(filePath + "Assets." + currentDocument + ".pdf");
                //Load the PDF
                pdfViewerControl.LoadDocument(fileStream);

                //if (!isPageSwitched)
                //{
                // pdfViewerControl.InputFileStream = (typeof(App).GetTypeInfo().Assembly.GetManifestResourceStream(filePath + "Assets." + currentDocument + ".pdf"));
                //(BindingContext as GettingStartedViewModel).PdfDocumentStream = (typeof(App).GetTypeInfo().Assembly.GetManifestResourceStream(filePath + "Assets." + currentDocument + ".pdf"));
                //}
            }
        }

        protected override void OnDisappearing()
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                backUpHorizontalOffset = pdfViewerControl.HorizontalOffset;
                backUpVerticalOffset = pdfViewerControl.VerticalOffset;
                backUpZoomFactor = pdfViewerControl.ZoomPercentage;
                pdfViewerControl.Unload();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                isPageSwitched = false;
            }
        }

        //private void PdfViewerControl_DocumentSaved(object sender, DocumentSaveInitiatedEventArgs args)
        //{
        //    string filePath = DependencyService.Get<ISave>().Save(args.SaveStream as MemoryStream);
        //    string message = "The PDF has been saved to " + filePath;
        //    DependencyService.Get<IAlertView>().Show(message);
        //}
       
    }
}