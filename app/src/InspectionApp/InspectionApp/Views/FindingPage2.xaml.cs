﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using InspectionApp.ViewModels;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FindingPage2 : ContentPage
    {
        public FindingPage2()
        {
            InitializeComponent();
            BindingContext = new VesselOwnerViewModel(this.Navigation);
        }

    }
}