﻿using InspectionApp.Services;
using Plugin.AudioRecorder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecordingPage : ContentPage
    {
        AudioRecorderService recorder;
        AudioPlayer player;
        bool isTimerRunning = false;
        int seconds = 0, minutes = 0;

        public RecordingPage()
        {
            InitializeComponent();
            if (Device.RuntimePlatform == Device.iOS)
                this.Padding = new Thickness(0, 28, 0, 0);

            recorder = new AudioRecorderService
            {
                StopRecordingAfterTimeout = true,
                TotalAudioTimeout = TimeSpan.FromSeconds(15),
                AudioSilenceTimeout = TimeSpan.FromSeconds(2)
            };

            player = new AudioPlayer();
            player.FinishedPlaying += Finish_Playing;
        }


            void Finish_Playing(object sender, EventArgs e)
            {
                btnRecord.IsEnabled = true;
                btnRecord.BackgroundColor = Color.FromHex("#7cbb45");
                btnPlay.IsEnabled = true;
                btnPlay.BackgroundColor = Color.FromHex("#7cbb45");
                btnStop.IsEnabled = false;
                btnStop.BackgroundColor = Color.Silver;
                lblSeconds.Text = "00";
                lblMinutes.Text = "00";
            }

            async void Record_Clicked(object sender, EventArgs e)
            {
                if (!recorder.IsRecording)
                {
                    seconds = 0;
                    minutes = 0;
                    isTimerRunning = true;
                    Device.StartTimer(TimeSpan.FromSeconds(1), () => {
                        seconds++;

                        if (seconds.ToString().Length == 1)
                        {
                            lblSeconds.Text = "0" + seconds.ToString();
                        }
                        else
                        {
                            lblSeconds.Text = seconds.ToString();
                        }
                        if (seconds == 60)
                        {
                            minutes++;
                            seconds = 0;

                            if (minutes.ToString().Length == 1)
                            {
                                lblMinutes.Text = "0" + minutes.ToString();
                            }
                            else
                            {
                                lblMinutes.Text = minutes.ToString();
                            }

                            lblSeconds.Text = "00";
                        }
                        return isTimerRunning;
                    });

                    //
                    recorder.StopRecordingOnSilence = IsSilence.IsToggled;
                    var audioRecordTask = await recorder.StartRecording();

                    btnRecord.IsEnabled = false;
                    btnRecord.BackgroundColor = Color.Silver;
                    btnPlay.IsEnabled = false;
                    btnPlay.BackgroundColor = Color.Silver;
                    btnStop.IsEnabled = true;
                    btnStop.BackgroundColor = Color.FromHex("#7cbb45");

                    await audioRecordTask;
                }
            }

            async void Stop_Clicked(object sender, EventArgs e)
            {
                StopRecording();
                await recorder.StopRecording();
            }

            void StopRecording()
            {
                isTimerRunning = false;
                btnRecord.IsEnabled = true;
                btnRecord.BackgroundColor = Color.FromHex("#7cbb45");
                btnPlay.IsEnabled = true;
                btnPlay.BackgroundColor = Color.FromHex("#7cbb45");
                btnStop.IsEnabled = false;
                btnStop.BackgroundColor = Color.Silver;
                lblSeconds.Text = "00";
                lblMinutes.Text = "00";
            }
            void Play_Clicked(object sender, EventArgs e)
            {
                try
                {
                    var filePath = recorder.GetAudioFilePath();

                    if (filePath != null)
                    {
                        StopRecording();
                        player.Play(filePath);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        
    }
}