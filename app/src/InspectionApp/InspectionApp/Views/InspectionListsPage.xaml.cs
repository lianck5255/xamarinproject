﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using InspectionApp.ViewModels;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InspectionListsPage : ContentPage
    {
        public InspectionListsPage()
        {
            InitializeComponent();
            BindingContext = new VesselOwnerViewModel(this.Navigation);
            
        }
    }
}