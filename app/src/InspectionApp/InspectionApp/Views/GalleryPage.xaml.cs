﻿using MediaManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InspectionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GalleryPage : ContentView
    {
        public GalleryPage()
        {
            
            InitializeComponent();
        }

        public object MediaFileType { get; private set; }
        private string videoUrl = "https://sec.ch9.ms/ch9/e68c/690eebb1-797a-40ef-a841-c63dded4e68c/Cognitive-Services-Emotion_high.mp4";
        private void BtnPlayVideo_Clicked(object sender, EventArgs e)
        {
            string url = "https://www.youtube.com/watch?v=BIritDZLYy0";
            MediaManager.CrossMediaManager.Current.Play(videoUrl);
            //MediaManager.CrossMediaManager.Current.Play(videoUrl);
            //MediaManager.CrossMediaManager.Current.Play(url, MediaFileType.Video);
        }

        private void BtnStopVideo_Clicked(object sender, EventArgs e)
        {
             MediaManager.CrossMediaManager.Current.Stop();
        }
    }
}