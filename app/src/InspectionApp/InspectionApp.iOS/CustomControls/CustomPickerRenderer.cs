﻿using InspectionApp.CustomControls;
using InspectionApp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace InspectionApp.iOS
{
    public class CustomPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            var element = (CustomPicker)Element;

            if (Control != null && Element != null && !string.IsNullOrEmpty(element.Icon))
            {
                var downarrow = UIImage.FromBundle(element.Icon);
                Control.RightViewMode = UITextFieldViewMode.Always;
                Control.RightView = new UIImageView(downarrow);
                Control.TextColor = UIColor.FromRGB(83, 63, 149);
                //Control.BackgroundColor = UIColor.Clear;
                Control.AttributedPlaceholder = new Foundation.NSAttributedString(Control.AttributedPlaceholder.Value, foregroundColor: UIColor.FromRGB(83, 63, 149));
                //Control.BorderStyle = UITextBorderStyle.RoundedRect;
                //Layer.BorderWidth = 1.0f;
                //Layer.CornerRadius = 4.0f;
                //Layer.MasksToBounds = true;
                //Layer.BorderColor = UIColor.FromRGB(83, 63, 149).CGColor;
            }
        }
    }
}