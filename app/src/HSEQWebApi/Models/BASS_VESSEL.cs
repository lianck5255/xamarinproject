using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HSEQ.Xamarin.WebApi.Controllers.Models
{
    [Table("DBO.BASS_VESSEL")]
    public partial class BASS_VESSEL
    {
        [Key]
        [StringLength(20)]
        public string VESSEL_ID { get; set; }

        [StringLength(35)]
        public string VESSELTRX_ID { get; set; }

        [StringLength(20)]
        public string COMPGRP_ID { get; set; }

        [StringLength(20)]
        public string DISTGRP_ID { get; set; }

        [StringLength(10)]
        public string DB_ID { get; set; }

        [StringLength(100)]
        public string TEMPLATE_ID { get; set; }

        [StringLength(35)]
        public string VESSEL_IMONO { get; set; }

        [StringLength(100)]
        public string VESSEL_NAME { get; set; }

        [StringLength(20)]
        public string VESSEL_TYPE { get; set; }

        [StringLength(100)]
        public string VESSEL_CLASS { get; set; }

        [StringLength(10)]
        public string VESSEL_SOCIETYCLASS { get; set; }

        [StringLength(4000)]
        public string VESSEL_FREETEXT { get; set; }

        [Column(TypeName = "numeric")]
        public int REC_DELETED { get; set; }

        [StringLength(10)]
        public string REC_REVDBID { get; set; }

        [StringLength(100)]
        public string REC_CREATOR { get; set; }

        public DateTime? REC_CREDATE { get; set; }

        [StringLength(100)]
        public string REC_REVISOR { get; set; }

        public DateTime? REC_REVDATE { get; set; }

        public DateTime? REC_REPLDATE { get; set; }

        [StringLength(100)]
        public string OTHER_VESSEL_NAME { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ISVESSEL { get; set; }

        [StringLength(50)]
        public string REFERENCE { get; set; }
    }
}
